<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'plevia' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'lHUMUbgB&(b`:s*A,1Dy`?DCsLx}qi;k=_)S)o{d<q3ymcIeMyHp&iZ722s?&[o^' );
define( 'SECURE_AUTH_KEY',  'ckvQJFioO93(%CP!`:)#s@Gm1w0H@<(DvM7E6NK=]rJueHk&7@#]p:Dd{a?@T$q`' );
define( 'LOGGED_IN_KEY',    'IOdU1{,t2P0~^!]b3sVS|WxXZ3L05hZ ~JPB6](^+YQ~e.(Cm8}*Ksyag<N!X~,D' );
define( 'NONCE_KEY',        'e-+kIcerT5RS]Aab30NsHBJ[ttT/j/Ok2bYPIn|CfI0J r`hR]euzw0(G3$w+?Bp' );
define( 'AUTH_SALT',        ':>(l_2t0@YaDZe|cq:SSHLW-cWh2Gxx_],wSnQFt<bP>0uG6^FxpC:ln!TNqBxzJ' );
define( 'SECURE_AUTH_SALT', '8Vp|Vj963Zo;(9W)MuK,a%i<36NDxK[+=3?<[1FJpcpLDT%)=;o3iJ+fYtd+=|0b' );
define( 'LOGGED_IN_SALT',   'XH#`P??~T d-EnNmeI)V7-a&RoU$J9Z?Ccon^QL(?#!f<dbKh[#nQMz:xf9$Ee+K' );
define( 'NONCE_SALT',       'j7V>7&ft;ay39`S:W0I% UhtTS:*7l>6>hzkkgF*~9O6KY{3|m-5NCLQoK6*WHQ7' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
// Tambahkan baris code ini pada file wp-config.php 
define('FS_METHOD', 'direct');

