<?php 
/* Template Name: Portfolio */
get_header(); ?>

  <!----HALAMAN PORTOFOLIO  -->
  <div class="container-portfolio wow fadeInLeft" data-wow-duration="1s" id="portfolio">
        <div class="container">
            <div class="portfolioFilter clearfix">
                <a href="#" data-filter="*" class="current">Semua Kategori</a>
                <a href="#" data-filter=".webContent">Web Content & Development</a>
                <a href="#" data-filter=".graphicDesign">Graphic Design</a>
                <a href="#" data-filter=".ui">UI/UX</a>
                <a href="#" data-filter=".webDesign">Web Design</a>
            </div>
        </div>

        <div class="container wow fadeInRight" data-wow-duration="1s">
            <div class="row portfolioContainer">
                <div class="graphicDesign">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/portofolio/gd_1.png" alt="image" class="img-portfolio img-fluid">
                </div>

                <div class="graphicDesign">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/portofolio/gd_2.jpg" alt="image" class="img-portfolio img-fluid">
                </div>

                <div class="webContent">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/portofolio/wd_1.jpg" alt="image" class="img-portfolio img-fluid">
                </div>

                <div class="webContent">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/portofolio/wd_2.jpg" alt="image" class="img-portfolio img-fluid">
                </div>



                <div class=" ui">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/portofolio/ui_1.png" alt="image" class="img-portfolio img-fluid">
                </div>

                <div class=" graphicDesign">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/portofolio/gd_3.jpg" alt="image" class="img-portfolio img-fluid">
                </div>

                <div class="webContent">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/portofolio/wd_3.jpg" alt="image" class="img-portfolio img-fluid">
                </div>

                <div class=" graphicDesign">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/portofolio/gd_4.jpg" alt="image" class="img-portfolio img-fluid">
                </div>
                
            </div>
        </div>
        <br>
    </div>
    <!---END HALAMAN PORTOFOLIO -->











<?php get_footer() ?>