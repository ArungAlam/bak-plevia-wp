<?php 
/* Template Name: laman_depan */
get_header(); ?>
<!-- Section  1 -->
<div id="main">
<div class="container-home" id="#home">
    <div class="container-fluid">
    <img class="bg-pertama" src="<?php echo get_template_directory_uri() ?>/assets/bg/bg-s2.png" >    
  
  
    <div class="bg-home">

            <img src="<?php echo get_template_directory_uri() ?>/assets/img/women-header.png" >    
  
    </div>
    <div class="spanBtn" >
        <ul class="lax "   data-lax-preset="driftLeft" data-lax-optimize=true style="margin-left: -3%" > 
            <a > <span class="spanbg s1" >Hujan  </span> </a>
        </ul>
        <ul class="lax"  data-lax-preset="driftRight"  data-lax-optimize=true> 
            <a><span class="spanbg s2">Pakai </span> </a> 
        </ul>
        <ul class="lax"  data-lax-preset="driftLeft" data-lax-optimize=true style="margin-left: -7%" > 
            <a> <span class="spanbg s3">Plevia  </span> </a> 
        </ul>
    </div>
</div>
</div>
<!-- END Section 1 -->

<!-- Section 2 -->
<div class="container-kategori" id="plevia">
    <div class="container-fluid" id="bg-abu">
            <div id="merah">
            <h1> Lorem Ingsum</h1>
            <p > remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
            </p>
 
            </div  >   
   <img class="lax img-model" data-lax-preset="eager-350" data-lax-optimize=true src="<?php echo get_template_directory_uri() ?>/assets/bg/model.png" > 
 
    </div>

    <div class="menu-kategori"> 
        <ul>
          <li>
            <a> Pria</a>
         </li>
          <li>
            <a> Wanita</a>
         </li>
         <li>
            <a> Anak</a>
         </li>
        </ul>
    </div>
  
</div>

<!-- END Section 2 -->



<!-- Section 3 -->
<div class="container-pria" id="produk">
    <h1 class="lax text-pria2" data-lax-preset="driftLeft" data-lax-optimize=true style="color:#30c1ec"><a >Pria  </a></h1> 
    <div class="gambar-pria">
            <div class="img-pria" > 
                 <h1 class="lax text-pria" data-lax-preset="driftLeft" data-lax-optimize=true style="color:#fff"><a >Pria  </a></h1> 
                 <img class="lax img-pria-model" data-lax-preset="eager-250 zoomOut" data-lax-optimize=true src="<?php echo get_template_directory_uri() ?>/assets/img/pria-container.png" > 
            </div>  
    </div>
        <div class="produk" style="align:center">
            <h2>Produk Ungulan</h2>
             <div class="d-flex">
                 <div class="p-2">
                     <img class="img-thumbnail" src="<?php echo get_template_directory_uri() ?>/assets/img/thumb-produk.png"> 
                </div>  <div class="p-2">
                    <img class="img-thumbnail" src="<?php echo get_template_directory_uri() ?>/assets/img/thumb-produk.png"> 
                </div>  <div class="p-2">
                    <img class="img-thumbnail" src="<?php echo get_template_directory_uri() ?>/assets/img/thumb-produk.png"> 
                 </div> <div class="ml-auto p-2" id="hilang">
                    <span class="spanbg  s4">lainnya </span>     
                </div>
            </div>
       </div>
        <div class="kelas-pria">
            <h1 class="lax ptext-bawah2" data-lax-preset="driftLeft" data-lax-optimize=true  style="color: #30c1ec;"><a >Ragam</h1> 
            <h1 class="lax ptext-bawah2" data-lax-preset="driftLeft" data-lax-optimize=true style="color:#30c1ec" ><a >Warna</h1>  
        </div>
        <div class="kelas-pria-luar">
        <img  class="bg-img-pria" src="<?php echo get_template_directory_uri() ?>/assets/img/pattern-pria-coba.png" >      

    </div>
    <div class="kelas-pria-dalam">
        <h1 class="lax ptext-bawah1"  data-lax-preset="driftLeft" data-lax-optimize=true style="color:red" ><a >Ragam</h1> 
        <h1 class="lax ptext-bawah1 v-2" data-lax-preset="driftLeft" data-lax-optimize=true style="color:red" ><a >Warna</h1>
    </div>
</div>

 <!-- END Section 3 -->
<!-- Section 4 -->
<div class="container-wanita" >
    <h1 class="lax text-wanita1"  data-lax-preset="driftRight" data-lax-optimize=true ><a style="color:red">Wanita </h1>
    <div class="container-wanita-luar">
        <div class="bg-wanita">
        <h1 class="lax text-wanita2" data-lax-preset="driftRight" data-lax-optimize=true >Wanita </h1> 
        <img class="lax img-wanita-model" data-lax-preset="eager-150 zoomOut" data-lax-optimize=true  src="<?php echo get_template_directory_uri() ?>/assets/img/wanita-img.png" > 
       
        </div>
    </div>

    <div class="produk-wanita" >
        <h2>Produk Ungulan</h2>
        <div class="d-flex flex-row-reverse">
            <div class="p-2">
            <img class="img-thumbnail" src="<?php echo get_template_directory_uri() ?>/assets/img/thumb-produk.png" > 
            </div>  <div class="p-2">
             <img  class="img-thumbnail" src="<?php echo get_template_directory_uri() ?>/assets/img/thumb-produk.png" > 
             </div>  <div class="p-2">
             <img class="img-thumbnail" src="<?php echo get_template_directory_uri() ?>/assets/img/thumb-produk.png" > 
            </div> <div class="mr-auto p-2" id="hilang">
             <span class="spanbg  s4">lainnya  </span>     
            </div>
   
        </div>
    </div>
    <div class="pattern-wanita">
    <h1 class="lax text-wanita-dalam1" data-lax-preset="driftLeft" data-lax-optimize=true style="color:red" ><a >Ragam </a> </h1> 
    <h1 class="lax text-wanita-dalam1" data-lax-preset="driftLeft" data-lax-optimize=true style="color:red"><a> Pattren</a></h1>    
</div>
    <div class="container-wanita-dalam">
        <div class="bg-wanita2">
            
         <h1 class="lax text-wanita-dalam2" data-lax-preset="driftLeft" data-lax-optimize=true style="color:blue"><a>Ragam</a> </h1> 
         <h1 class="lax text-wanita-dalam2 b-2" data-lax-preset="driftLeft" data-lax-optimize=true style="color:blue"><a >Pattren</a></h1>    
        </div>
    </div>
</div>

 <!-- END Section 4 -->
<!-- Section 5 -->
<div class="container-anak">
        <h1 class="lax text-anak1" data-lax-preset="driftLeft" data-lax-optimize=true style="color:#30c1ec" >Anak </h1> 

        <div class="gambar-anak">
       <h1 class="lax text-anak2" data-lax-preset="driftLeft" data-lax-optimize=true  style="color:red" >Anak </h1> 
       <img class="lax img-anak-model"  data-lax-preset="eager-150 zoomOut" data-lax-optimize=true  src="<?php echo get_template_directory_uri() ?>/assets/img/kids-img.png" > 
           
    </div >
  
    <div class="produk-anak">
            <h2>Produk Ungulan</h2>
        <div class="d-flex">
            <div class="p-2">
            <img class="img-thumbnail" src="<?php echo get_template_directory_uri() ?>/assets/img/thumb-produk.png" > 
            </div>  <div class="p-2">
             <img class="img-thumbnail" src="<?php echo get_template_directory_uri() ?>/assets/img/thumb-produk.png" > 
             </div>  <div class="p-2">
             <img class="img-thumbnail" src="<?php echo get_template_directory_uri() ?>/assets/img/thumb-produk.png" > 
            </div> <div class="ml-auto p-2" id="hilang">
             <span class="spanbg  s4">lainnya  </span>     
            </div>
   
        </div>
    </div>
    <div class="kelas-anak-luar">
    <h1 class="lax text-anak-dalam1" data-lax-preset="driftLeft" data-lax-optimize=true  >Fitur</h1> 
    <div class="kelas-anak-dalam">
        <h1 class="lax text-anak-dalam2" data-lax-preset="driftLeft" data-lax-optimize=true >Fitur</h1> 
    </div>
</div>
</div>

<!-- END Section 5 -->
<!-- Section 6 -->
<div class="container-tentang">
<div class="container-fluid">
<div class="rotate"  >
<img class="lax"  data-lax-preset="x" data-lax-optimize=true  src="<?php echo get_template_directory_uri() ?>/assets/img/g.png" > 
   
</div>

<div class="bg-tentang"  id="tentang">
      <div class="bg-biru">
            <h1> Plevia</h1>
            <p>Alamat:Jl.Puter Distrik RT 04 / RW </br>
                03 Jetak ,Wonorejo, Gondangrejo,</br>
                Karanganyar</br>
                Email : plevia.id@gmail.com </br>
                Telp  : +62 271 8500 311 </br>  
                </br>
                </br>
            </p>
            <ul >
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-globe"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                            </ul>
        </div>
</div>
</div>
</div>


<!-- END Section 6 -->
</div>
<?php get_footer() ?>
