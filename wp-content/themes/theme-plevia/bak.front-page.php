<?php get_header(); ?>
    
    
     <!--  HALAMAN SLIDER -->
          <div id="rev_slider_11_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="restaurant" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
           <!-- START REVOLUTION SLIDER 5.4.6.3.1 fullwidth mode -->
            <div id="rev_slider_11_1" class="rev_slider " style="display:none;" data-version="5.4.6.3.1">
                <ul>
                    <!-- SLIDE  -->
                    <li data-index="rs-36" data-transition="3dcurtain-horizontal,3dcurtain-vertical,cube-horizontal,turnoff,parallaxtoright,parallaxtotop,parallaxhorizontal,slotfade-horizontal,slidingoverlayleft" data-slotamount="default,default,default,default,default,default,default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-randomtransition="on" data-easein="default,default,default,default,default,default,default,default,default,default" data-easeout="default,default,default,default,default,default,default,default,default,default" data-masterspeed="default,default,default,default,default,default,default,default,default,default" data-thumb="assets/slider_restaurant_slide_bg_01-100x50.jpg" data-rotate="0,0,0,0,0,0,0,0,0,0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/slider/2.jpg" alt="" title="slider_restaurant_slide_bg_01" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <h1 class="tp-caption   tp-resizeme" id="slide-93-layer-3" data-x="['left','center','center','center']" data-hoffset="['143','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-47','17','15','0']" data-fontsize="['70','85','70','50']" data-lineheight="['85','93','77','55']" data-letterspacing="['0','0','','']" data-width="['750','800','600','430']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1700,"speed":2000,"sfxcolor":"#edeb03","sfx_effect":"blockfrombottom","frame":"0","from":"z:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['left','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; min-width: 750px; max-width: 750px; white-space: normal; font-size: 70px; line-height: 85px; font-weight: 700; color: #ffffff; letter-spacing: 0px;text-align:left !important;"><span class="secondary-font">
                        Keajabian Sosial Media</span> </h1>

                        <!-- LAYER NR. 2 -->
                        <a href="#">
                    <div class="tp-caption Flat-Primary rev-btn " id="slide-93-layer-23" data-x="['center','center','center','center']" data-hoffset="['-426','-3','3','1']" data-y="['middle','middle','middle','middle']" data-voffset="['155','229','229','171']" data-width="none" data-height="56" data-whitespace="nowrap" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":10,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(41,53,207);bg:rgb(255,255,255);bs:solid;bw:0 0 0 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[35,35,35,35]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[35,35,35,35]" style="z-index: 6; max-width: 56px; max-width: 56px; white-space: nowrap; font-size: 14px; line-height: 55px; font-weight: 700; color: #ffffff; letter-spacing: 1.4px;font-family:Work Sans;text-transform:uppercase;background-color:rgb(56,22,183);border-color:transparent;border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Pelajari Selengkapnya </div></a>                    </li>
                    <!-- SLIDE  -->
                    <li data-index="rs-30" data-transition="3dcurtain-horizontal,3dcurtain-vertical,cube-horizontal,turnoff,parallaxtoright,parallaxtotop,parallaxhorizontal,slotfade-horizontal,slidingoverlayleft" data-slotamount="default,default,default,default,default,default,default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-randomtransition="on" data-easein="default,default,default,default,default,default,default,default,default,default" data-easeout="default,default,default,default,default,default,default,default,default,default" data-masterspeed="default,default,default,default,default,default,default,default,default,default" data-thumb="assets/slider_restaurant_slide_bg_02-100x50.jpg" data-rotate="0,0,0,0,0,0,0,0,0,0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/slider/1.jpg" alt="" title="slider_restaurant_slide_bg_02" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 4 -->
                        <h1 class="tp-caption   tp-resizeme" id="slide-93-layer-3" data-x="['left','center','center','center']" data-hoffset="['143','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-47','17','15','0']" data-fontsize="['70','85','70','50']" data-lineheight="['85','93','77','55']" data-letterspacing="['0','0','','']" data-width="['750','800','600','430']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1700,"speed":2000,"sfxcolor":"#edeb03","sfx_effect":"blockfrombottom","frame":"0","from":"z:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['left','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; min-width: 750px; max-width: 750px; white-space: normal; font-size: 70px; line-height: 85px; font-weight: 700; color: #ffffff; letter-spacing: 0px;text-align:left !important;"><span class="secondary-font">
                        Keajabian Sosial Media</span> </h1>

                        <!-- LAYER NR. 5 -->
                        <a href="#">
                    <div class="tp-caption Flat-Primary rev-btn " id="slide-93-layer-23" data-x="['center','center','center','center']" data-hoffset="['-426','-3','3','1']" data-y="['middle','middle','middle','middle']" data-voffset="['155','229','229','171']" data-width="none" data-height="56" data-whitespace="nowrap" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":10,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(41,53,207);bg:rgb(255,255,255);bs:solid;bw:0 0 0 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[35,35,35,35]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[35,35,35,35]" style="z-index: 6; max-width: 56px; max-width: 56px; white-space: nowrap; font-size: 14px; line-height: 55px; font-weight: 700; color: #ffffff; letter-spacing: 1.4px;font-family:Work Sans;text-transform:uppercase;background-color:rgb(56,22,183);border-color:transparent;border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Pelajari Selengkapnya </div></a>                    </li>
                     </li>
                     <!-- SLIDE  -->
                    <li data-index="rs-32" data-transition="3dcurtain-horizontal,3dcurtain-vertical,cube-horizontal,turnoff,parallaxtoright,parallaxtotop,parallaxhorizontal,slotfade-horizontal,slidingoverlayleft" data-slotamount="default,default,default,default,default,default,default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-randomtransition="on" data-easein="default,default,default,default,default,default,default,default,default,default" data-easeout="default,default,default,default,default,default,default,default,default,default" data-masterspeed="default,default,default,default,default,default,default,default,default,default" data-thumb="assets/slider_restaurant_slide_bg_02-100x50.jpg" data-rotate="0,0,0,0,0,0,0,0,0,0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/slider/3.jpg" alt="" title="slider_restaurant_slide_bg_02" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 4 -->
                        <h1 class="tp-caption   tp-resizeme" id="slide-93-layer-3" data-x="['left','center','center','center']" data-hoffset="['143','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-47','17','15','0']" data-fontsize="['70','85','70','50']" data-lineheight="['85','93','77','55']" data-letterspacing="['0','0','','']" data-width="['750','800','600','430']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1700,"speed":2000,"sfxcolor":"#edeb03","sfx_effect":"blockfrombottom","frame":"0","from":"z:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['left','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; min-width: 750px; max-width: 750px; white-space: normal; font-size: 70px; line-height: 85px; font-weight: 700; color: #ffffff; letter-spacing: 0px;text-align:left !important;"><span class="secondary-font">
                        Identitasmu memikat.</span> 
                        <span class="secondary-font">Customermu melekat.</span> 
                        </h1>

                        <!-- LAYER NR. 5 -->
                        <a href="#">
                    <div class="tp-caption Flat-Primary rev-btn " id="slide-93-layer-23" data-x="['center','center','center','center']" data-hoffset="['-426','-3','3','1']" data-y="['middle','middle','middle','middle']" data-voffset="['155','229','229','171']" data-width="none" data-height="56" data-whitespace="nowrap" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":10,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(41,53,207);bg:rgb(255,255,255);bs:solid;bw:0 0 0 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[35,35,35,35]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[35,35,35,35]" style="z-index: 6; max-width: 56px; max-width: 56px; white-space: nowrap; font-size: 14px; line-height: 55px; font-weight: 700; color: #ffffff; letter-spacing: 1.4px;font-family:Work Sans;text-transform:uppercase;background-color:rgb(56,22,183);border-color:transparent;border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Pelajari Selengkapnya </div></a>                    </li>
                     </li>
                     <!-- SLIDE  -->
                    <li data-index="rs-37" data-transition="3dcurtain-horizontal,3dcurtain-vertical,cube-horizontal,turnoff,parallaxtoright,parallaxtotop,parallaxhorizontal,slotfade-horizontal,slidingoverlayleft" data-slotamount="default,default,default,default,default,default,default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-randomtransition="on" data-easein="default,default,default,default,default,default,default,default,default,default" data-easeout="default,default,default,default,default,default,default,default,default,default" data-masterspeed="default,default,default,default,default,default,default,default,default,default" data-thumb="assets/slider_restaurant_slide_bg_02-100x50.jpg" data-rotate="0,0,0,0,0,0,0,0,0,0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/slider/4.jpg" alt="" title="slider_restaurant_slide_bg_02" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 4 -->
                        <h1 class="tp-caption   tp-resizeme" id="slide-93-layer-3" data-x="['left','center','center','center']" data-hoffset="['143','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-47','17','15','0']" data-fontsize="['70','85','70','50']" data-lineheight="['85','93','77','55']" data-letterspacing="['0','0','','']" data-width="['750','800','600','430']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1700,"speed":2000,"sfxcolor":"#edeb03","sfx_effect":"blockfrombottom","frame":"0","from":"z:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['left','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; min-width: 750px; max-width: 750px; white-space: normal; font-size: 70px; line-height: 85px; font-weight: 700; color: #ffffff; letter-spacing: 0px;text-align:left !important;"><span class="secondary-font">
                        Tidak hanya menjual.</span>
                        <span class="secondary-font">Kamipun bercerita.</span>
                         </h1>

                        <!-- LAYER NR. 5 -->
                        <a href="#">
                    <div class="tp-caption Flat-Primary rev-btn " id="slide-93-layer-23" data-x="['center','center','center','center']" data-hoffset="['-426','-3','3','1']" data-y="['middle','middle','middle','middle']" data-voffset="['155','229','229','171']" data-width="none" data-height="56" data-whitespace="nowrap" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":10,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(41,53,207);bg:rgb(255,255,255);bs:solid;bw:0 0 0 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[35,35,35,35]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[35,35,35,35]" style="z-index: 6; max-width: 56px; max-width: 56px; white-space: nowrap; font-size: 14px; line-height: 55px; font-weight: 700; color: #ffffff; letter-spacing: 1.4px;font-family:Work Sans;text-transform:uppercase;background-color:rgb(56,22,183);border-color:transparent;border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Pelajari Selengkapnya </div></a>                    </li>
                     </li>
                     <!-- SLIDE  -->
                    <li data-index="rs-38" data-transition="3dcurtain-horizontal,3dcurtain-vertical,cube-horizontal,turnoff,parallaxtoright,parallaxtotop,parallaxhorizontal,slotfade-horizontal,slidingoverlayleft" data-slotamount="default,default,default,default,default,default,default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-randomtransition="on" data-easein="default,default,default,default,default,default,default,default,default,default" data-easeout="default,default,default,default,default,default,default,default,default,default" data-masterspeed="default,default,default,default,default,default,default,default,default,default" data-thumb="assets/slider_restaurant_slide_bg_02-100x50.jpg" data-rotate="0,0,0,0,0,0,0,0,0,0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/slider/5.jpg" alt="" title="slider_restaurant_slide_bg_02" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 4 -->
                        <h1 class="tp-caption   tp-resizeme" id="slide-93-layer-3" data-x="['left','center','center','center']" data-hoffset="['143','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-47','17','15','0']" data-fontsize="['70','85','70','50']" data-lineheight="['85','93','77','55']" data-letterspacing="['0','0','','']" data-width="['750','800','600','430']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1700,"speed":2000,"sfxcolor":"#edeb03","sfx_effect":"blockfrombottom","frame":"0","from":"z:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['left','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; min-width: 750px; max-width: 750px; white-space: normal; font-size: 70px; line-height: 85px; font-weight: 700; color: #ffffff; letter-spacing: 0px;text-align:left !important;"><span class="secondary-font">
                        Tidak hanya menjual.</span>
                        <span class="secondary-font">Kamipun bercerita.</span>
                         </h1>
                        <!-- LAYER NR. 5 -->
                        <a href="#">
                    <div class="tp-caption Flat-Primary rev-btn " id="slide-93-layer-23" data-x="['center','center','center','center']" data-hoffset="['-426','-3','3','1']" data-y="['middle','middle','middle','middle']" data-voffset="['155','229','229','171']" data-width="none" data-height="56" data-whitespace="nowrap" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":10,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(41,53,207);bg:rgb(255,255,255);bs:solid;bw:0 0 0 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[35,35,35,35]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[35,35,35,35]" style="z-index: 6; max-width: 56px; max-width: 56px; white-space: nowrap; font-size: 14px; line-height: 55px; font-weight: 700; color: #ffffff; letter-spacing: 1.4px;font-family:Work Sans;text-transform:uppercase;background-color:rgb(56,22,183);border-color:transparent;border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Pelajari Selengkapnya </div></a>                    </li>
                    </li>
                    
                </ul>
                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
            </div>
        </div>
    <!-- END HALAMAN SLIDER -->
    
    
    
    <!-- HALAMAN SERVICES -->
    <div class="container-fluid service-container" id="services">
        <div class="section-header header-has-bg wow bounceInDown" data-wow-duration="1s">
            <h2 class="section-title">
                <span class="has-border-top">Sugarad Creative Digital Communication</span>
            </h2>
            <p class="section-title-alias">
            CREATIVE DIGITAL COMMUNICATION AND BRAND CONSULTANT
            </p>
            <br>
            <br>
                <a href="<?php get_template_directory_uri('section/about') ?>" class="btn btn-primary"> 
                    Selengkapnya
                </a>
        </div>
        <div class="row">
            <div class="col-md-2 offset-md-1 service-item wow bounceInUp" data-wow-duration="2s">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/services/Social%20Media.png" class="img-fluid">
                <h5 class="service-item-title">Social Media Marketing</h5>
            </div>
            <div class="col-md-2 service-item wow bounceInUp" data-wow-duration="2s" data-wow-delay="0.5s">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/services/Branding.png" class="img-fluid">
                <h5 class="service-item-title">Branding</h5>
            </div>
            <div class="col-md-2 service-item wow bounceInUp" data-wow-duration="2s" data-wow-delay="1s">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/services/Marketing%20Strategy.png" class="img-fluid">
                <h5 class="service-item-title">Marketing Strategy</h5>
            </div>
            <div class="col-md-2 service-item wow bounceInUp" data-wow-duration="2s" data-wow-delay="1.5s">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/services/Photo%20&%20Video.png" class="img-fluid">
                <h5 class="service-item-title">Photo & Video</h5>
            </div>
            <div class="col-md-2 service-item wow bounceInUp" data-wow-duration="2s" data-wow-delay="2s">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/services/Web%20Design.png" class="img-fluid">
                <h5 class="service-item-title">Web Design</h5>
            </div>
        </div>
    </div>
    <!-- END HALAMAN SERVICES -->
    
  
    
     <!----HALAMAN Team Kami -->
    <div class="container-fluid team-container">
        <div class="section-header header-has-bg wow bounceInDown" data-wow-duration="1s">
            <h2 class="section-title">
                <span class="has-bg-color">Tim kami</span>
            </h2>
            <p class="section-title-alias">
            Dengan orang-orang yang luar biasa, kami berusaha memberikan layanan yang terbaik.
            </p>
        </div>
                    <div class="container">
            <div class="row row-team">
                <!--img per team-->
                <div class="container-img">
                    <div class="content">
                        <div class="content-overlay"></div>
                        <img class="content-image" src="<?php echo get_template_directory_uri() ?>/assets/img/team/firhat.jpg">
                        <div class="content-details fadeIn-right">
                            <h4 class="content-title">
                                <span class="has-border-top">MUHAMMAT</span>
                                <span class="has-bg-color">FIRHAT</span>
                            </h4>
                            <p class="content-text">IT Consultant</p>
                            <ul>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-globe"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div><!-- //eof img-container -->
                <!--img per team-->
                <div class="container-img">
                    <div class="content">
                        <div class="content-overlay"></div>
                        <img class="content-image" src="<?php echo get_template_directory_uri() ?>/assets/img/team/afni.jpg">
                        <div class="content-details fadeIn-right">
                            <h4 class="content-title">
                                <span class="has-border-top">AFNI</span>
                                <span class="has-bg-color">ARSAD</span>
                            </h4>
                            <p class="content-text">Graphic Designer</p>
                            <ul>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-globe"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div><!-- //eof img-container -->
    
                 <!--img per team-->
                <div class="container-img">
                    <div class="content">
                        <div class="content-overlay"></div>
                        <img class="content-image" src="<?php echo get_template_directory_uri() ?>/assets/img/team/putra.jpg">
                        <div class="content-details fadeIn-right">
                            <h4 class="content-title">
                                <span class="has-border-top">A RHADITSTYA</span>
                                <span class="has-bg-color">PUTRA</span>
                            </h4>
                            <p class="content-text">Branding Experts</p>
                            <ul>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-globe"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- //eof img-container -->
                  <!--img per team-->
                  <div class="container-img">
                    <div class="content">
                        <div class="content-overlay"></div>
                        <img class="content-image" src="<?php echo get_template_directory_uri() ?>/assets/img/team/ria.jpg">
                        <div class="content-details fadeIn-right">
                            <h4 class="content-title">
                                <span class="has-border-top">RIA</span>
                                <span class="has-bg-color">AVRIYANTI</span>
                            </h4>
                            <p class="content-text">Admin</p>
                            <ul>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-globe"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div><!-- //eof img-container -->
                 
                  <!--img per team-->
                  <div class="container-img">
                    <div class="content">
                        <div class="content-overlay"></div>
                        <img class="content-image" src="<?php echo get_template_directory_uri() ?>/assets/img/team/dimas.jpg">
                        <div class="content-details fadeIn-right">
                            <h4 class="content-title">
                                <span class="has-border-top">DIMAS</span>
                                <span class="has-bg-color">GATSU</span>
                            </h4>
                            <p class="content-text">Copywriter</p>
                            <ul>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-globe"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- //eof img-container -->    
                 <!--img per team-->
                <div class="container-img">
                    <div class="content">
                        <div class="content-overlay"></div>
                        <img class="content-image" src="<?php echo get_template_directory_uri() ?>/assets/img/team/senja.jpg">
                        <div class="content-details fadeIn-right">
                            <h4 class="content-title">
                                <span class="has-border-top">SENJA</span>
                                <span class="has-bg-color">KURNIA</span>
                            </h4>
                            <p class="content-text">Copywriter</p>
                            <ul>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-globe"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div><!-- //eof img-container -->    
            </div>
        </div>
            
        
    </div>
    <!---END HALAMAN Team Kami -->
    
   
<!--
    <div class="map-container">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15815.68478930662!2d110.87309149825072!3d-7.6916063471750435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a3b61d726801b%3A0x343ccacd4c4ea354!2zN8KwNDEnMTguOSJTIDExMMKwNTInMzIuMSJF!5e0!3m2!1sen!2sid!4v1535524731042" frameborder="0" style="border:0; width: 100%; height: 450px;" allowfullscreen></iframe>
    </div>
-->

   
    
<?php get_footer() ?>