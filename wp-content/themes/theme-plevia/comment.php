 <div class="comment-section">
 <?php if (comments_open()){?> 

  <?php if (get_option('comment_registration') && !$user_ID) { ?>
  <p>You must be <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>">logged in</a> to post a comment.</p>
  <?php } else { ?>
  <?php } ?>
                        <h3 class="article-title">
                            Write a comment
                        </h3>
                        
                        <div class="container form-comment-container">
                            <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php">
                            <?php wp_nonce_field( 'comment_nonce' ); 
    if ($user_ID) {
      ?>
        <div class="logged_comment_links">Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>.
          <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="Log out of this account">Log out &raquo;</a>
        </div>
      <?php
    } else {
      ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control form-comment" placeholder="Name *"name="author">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="email" class="form-control form-comment" placeholder="Email * " name="email">
                                    </div>
                                </div> 
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                    <?php comment_id_fields(); ?>
                                        <textarea class="form-control form-comment" placeholder="Your comment..." name="comment"></textarea>
                                    </div>
                                </div>
                                <br>
                                <?php
    } ?>
                                <center>
                                    <button class="button btn-gradient">
                                        Submit comment
                                    </button>
                                </center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
 <?php }?>