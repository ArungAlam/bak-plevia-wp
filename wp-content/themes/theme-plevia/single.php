<?php get_header(); 
if (have_posts() ):
		while(have_posts()): the_post();
?>

<nav class="navbar navbar-expand-lg navbar-light bg-green fixed-top green-nav">
            <a class="navbar-brand" href="index.html">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo/sugarad-logo.png" width="200" height="70" class="d-inline-block align-top" alt="">
                
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link scroll-trigger" href="<?php echo "http://localhost/cek/" ?>">Kembali ke Beranda<span class="sr-only">(current)</span></a>

                    </li>
                </ul>
            </div>
        </nav>

    <article class="article-section">
        <div class="container">
            <div class="row">
                <div class="col-md-4 article-sidebar">
                    <div class="sidebar-search">
                        <div class="input-group">
                            <input type="text" class="form-control form-search-control" placeholder="Search...">
                            <div class="input-group-append">
                                <button class="btn btn-search" type="button">
                                    <i class="ti-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
              
                    <div class="sidebar-latest">
                        <h4>Artikel Terbaru</h4>
    <?php
	$args = array( 'numberposts' => '3' );
	$recent_posts = wp_get_recent_posts( $args );
	foreach( $recent_posts as $recent ){
		echo '<div class="latest-blog-item">
        <div class="row">
            <div class="col-md-5">
                <a href="' . get_permalink($recent["ID"]) . '">
                ' .  get_the_post_thumbnail($recent["ID"],'thumbnail', array('class' => 'img-small-latest')) .'
                </a>
            </div>
            <div class="col-md-7">
                <h6>
                    <a>
                        '.$recent["post_title"].'
                    </a>
                </h6>
                <small> '. get_the_date('j-F-Y' ,$recent["ID"]).'</small>
            </div>
        </div>
    </div>';

    }
	wp_reset_query();
?>   
                    </div>
                    
                    <div class="sidebar-tags">
                        
                     </div>
                </div>
                <div class="col-md-8">
                    <div class="article-header">
                        <h3 class="article-title">
                        <?php the_title(); ?>
                        </h3>
                        <h6 class="article-info">
                            March 20, 2018 <span>in</span>
                            <a href="#" class="article-cat-l">
                                Web Design
                            </a>
                            <a href="#" class="article-cat-l">
                                Company
                            </a>
                        </h6>
                    </div>
                    <div class="article-main">
                    <?php the_post_thumbnail('medium', array('class' => 'img-article-main')); ?>

                        <p class="article-text">
                        <?php the_content(); ?>
                        </p>
                    </div>
                    <div class="article-footer">
                    <div class="row row-tags">
                    <div class="col-md-6">
                  <!---      
                            
                                <strong>Category : </strong> &nbsp;
                                <a href="#" class="article-cat-l">
                                    Web Design
                                </a>
                                <a href="#" class="article-cat-l">
                                    Company
                                </a>
                            </div>
                            
                     -->
                            </div>
                        </div>
                        
                        <h3 class="article-title">
                            Artikel Terkait
                        </h3>

                        <div class="related-post-container">
                            <div class="row article-items">
                            <?php
	$args = array( 'numberposts' => '3' );
	$recent_posts = wp_get_recent_posts( $args );
	foreach( $recent_posts as $recent ){ ?>
                <div class="col-md-4" >
                    <div class="card-container">
                        <div class="card">
                            <div class="front">
                                <div class="header">
                                    <ul class="blog-cat-l">
                                        <li>
                                            <a href="#">Blog</a>
                                        </li>
                                        <li>
                                            <a href="#">Web Design</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="content">
                                    <div class="main">
                                        <h4 class="front-title">
											<?php get_the_title($recent["ID"]); ?>
                                        </h4>
                                    </div>
                                    <p class="text-blog-l">
                                        by <strong><?php get_the_author_posts_link($recent["ID"]); ?></strong> &nbsp;/&nbsp; <?php get_the_time($recent["ID"],'F jS, Y'); ?>
                                    </p>
                                </div>
                            </div>
                            <!-- end front panel -->
                            <div class="back">
                                <div class="img-overlay">
                                    <img src="<?php echo get_the_post_thumbnail_url($recent["ID"]) ?>" class="img-fit" />
                                    <div class="project-overlay">
                                        <div class="header">
                                            <ul class="blog-cat-l-flipped">
                                                <li>
                                                    <a href="#">Blog</a>
                                                </li>
                                                <li>
                                                    <a href="#">Web Design</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="content">
                                            <div class="main">
                                                <h4>
                                                    <a href="<?php get_the_permalink(); ?>" class="link-flipped">
														<?php the_title(); ?>
                                                    </a>
                                                </h4>
                                            </div>
                                            <p class="text-blog-l-flipped">
												by <strong><?php the_author_posts_link($recent["ID"]); ?></strong> &nbsp;/&nbsp; <?php get_the_time($recent["ID"],'F jS, Y'); ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end back panel -->
                        </div>
                        <!-- end card -->
                    </div>
                    <!-- end card-container -->
                </div>
                <!-- end col md 4 -->
             
                <?php    ;

}
wp_reset_query();
  ?>


                            </div>
                        </div>
                    </div>
                    
             
        
    </article>

<?php 
			
					comments_template('/comment.php');
				
endwhile;
else:
        echo 'tidak ada post';
endif;

?>
 </footer>
    
    <div class="bottom-bar"></div>

    
<?php get_footer() ?>