	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- Bootstrap Core Engine -->
    </body>    
    <footer>
        <div class="container-footer">
           <h2>hak cipta &copy; 2019 Plevia Makmur Abadi</h2>
        </div>
    </footer>
    

    <script src="<?php echo get_template_directory_uri() ?>/assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/assets/js/popper.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/assets/js/bootstrap.min.js"></script>

    <!-- Typed.js -->
    <script src="<?php echo get_template_directory_uri() ?>/assets/js/typed.min.js"></script>

    <!-- Slick -->
    <script src="<?php echo get_template_directory_uri() ?>/assets/js/easing.js"></script>

    <script src="<?php echo get_template_directory_uri() ?>/assets/js/slick/slick.min.js"></script>

   
    <!-- Isotope -->
    <script src='https://isotope.metafizzy.co/v1/jquery.isotope.min.js'></script>

    <!-- Wow Js -->
    <script src="<?php echo get_template_directory_uri() ?>/assets/js/wow.min.js"></script>

    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/js/revo/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/js/revo/jquery.themepunch.revolution.min.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading)     -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/js/revo/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/js/revo/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/js/revo/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/js/revo/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/js/revo/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/js/revo/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/js/revo/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/js/revo/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/js/revo/extensions/revolution.extension.video.min.js"></script>

    <!-- Custom js -->
    <script src="<?php echo get_template_directory_uri() ?>/assets/js/script.js"></script>
   



</html>