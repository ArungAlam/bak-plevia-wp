<?php
function init_setup(){

    register_nav_menu('top', 'Top menu');
    add_theme_support( 'post-thumbnails' );
    add_image_size ('small_thumb');
    add_image_size ('big_thumb');
    add_theme_support('post-formats',array('aside','gallery') );
    
    
    }
    add_action('after_setup_theme','init_setup');
function mytheme_comment($comment, $args, $depth){
        $GLOBALS['comment'] = $comment;
        extract($args, EXTR_SKIP);
        $comment_id = get_comment_ID();
        $comment_author_name = get_comment_author($comment_id);
        $comment_author_email = get_comment_author_email();
        $get_comment_author_url = get_comment_author_url();
        ?>
      <div class="single_comment col-xs-12 nlp" <?php echo $tag; ?> <?php comment_class(empty($args['has_children']) ? '' : 'parent') ?> id="comment-<?php echo $comment_id; ?>">
        <div class="comment_photo col-xs-12 nlp">
          <img src="<?php echo getavatar_src($comment_author_email,100);?>" />
        </div>
        <div class="comment_data_area col-xs-10 nlp">
        <h3><?php echo $comment_author_name; ?></h3>
        <?php if ($comment->comment_approved == '0'){?>
          <em class="col-xs-12 comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
          <br/>
        <?php } ?>
        <p><?php comment_text();?></p>  
         
        <div class="reply">
        <?php 
        comment_reply_link( 
          array_merge( 
            $args, 
            array( 
              'depth'     => $depth, 
              'max_depth' => $args['max_depth'] 
            ) 
          ) 
        ); ?>
        </div>
      </div>
      </div>
    <?php
    } 
    ?>