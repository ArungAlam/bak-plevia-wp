<?php 
/* Template Name: Kontak versi 2 */
get_header(); 
if (have_posts() ):
        while(have_posts()): the_post();
        ?>
<div class="container-kontak">
    <div class="container-fluid ">
          <h1> Hubungi Kami</h1> 
         <div class="row justify-content-md-center" >
            <div class="col-md-10 ">   
                 <?php the_content(); ?>
            </div>
         </div>

        <div class="row justify-content-md-center" >
            <div class="col-md-12 ">
                <!--- embel google map -->
                <div class="map-container">
                      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15815.68478930662!2d110.87309149825072!3d-7.6916063471750435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a3b61d726801b%3A0x343ccacd4c4ea354!2zN8KwNDEnMTguOSJTIDExMMKwNTInMzIuMSJF!5e0!3m2!1sen!2sid!4v1535524731042" frameborder="0" style="border:0; width: 100%; height: 450px;" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
<pre>
</div>

<?php
endwhile;
else:
        echo 'tidak ada post';
endif;
?>

<!-- End section-->
<?php get_footer() ?>