<!DOCTYPE html>

<html lang="id">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Plevia.id</title>

    <!-- Favicon -->
    <link rel="icon" href="<?php echo get_template_directory_uri() ?>/assets/img/logo/favicon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/bootstrap.min.css">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=montserrat:400,700" rel="stylesheet">
  
     <link href="http://netdna.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Themify Icons -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/themify-icons.css">

    <!-- Slick Carousel -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/js/slick/slick.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/js/slick/slick-theme.css">

    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/assets/css/revo.css">

    <!-- Animate -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/animate.css">

    <!-- AOS -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/aos.css">

    <!-- Custom -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/style.css">
    <!-- Laxx js-->
    <script src="<?php echo get_template_directory_uri() ?>/laxxx/lib/lax.min.js"></script>
    <script type="text/javascript">
    window.onload = function() {
      document.getElementById("main").classList.add("loaded")

      lax.setup()

      const update = () => {
        lax.update(window.scrollY)
        window.requestAnimationFrame(update)
      }

      window.requestAnimationFrame(update)

      let w = window.innerWidth
      window.addEventListener("resize", function() {
        if(w !== window.innerWidth) {
          lax.updateElements()
        }
      });
    }
  </script>

</head>

<body>

    
    <header id="home">
        <nav class="navbar navbar-expand-lg navbar-light bg-green fixed-top">
            <a class="navbar-brand" >
                <img src="<?php echo get_template_directory_uri() ?>/assets/bg/logo_plevia.png" width="200" height="70" class="d-inline-block align-top" alt="">
                
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link scroll-trigger" href="<?php echo get_site_url(); ?>">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link scroll-trigger" href="#plevia">plevia </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link scroll-trigger" href="#produk">produk</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link scroll-trigger"href="<?php echo get_site_url() .'/blog' ?>">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link scroll-trigger" href="#tentang">tentang</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link scroll-trigger" href="<?php echo get_site_url() .'/kontak' ?>">Kontak</a>
                    </li>
                </ul>
            </div>
        </nav>

</header>