<?php 
/* Template Name: blog */
get_header(); ?>


    <div class="container-fluid blog-container wow bounceInDown" data-wow-duration="2s" id="blog">
        <div class="section-header header-has-bg">
            <h2 class="section-title">
               <span>Artikel</span>
            </h2>
            <p class="section-title-alias">
      </div>
        <div class="container row-blog wow bounceInUp" data-wow-duration="2s">
            <div class="row">

                <?php 
                $args = array(
                    'post_type' => 'post',
                     'post_status' => 'publish',
                     'posts_per_page' => 10,
                     'paged' => $pages,
                   );
                    
                    $arr_posts = new wp_Query($args);
                if ($arr_posts -> have_posts()):
                    while($arr_posts -> have_posts()):
                    $arr_posts -> the_post();
                    ?>
                

               <div class="col-md-4 blog-item-thumb">
                    <div class="main-blog-item" style="padding-bottom: 25px;">
                        <div class="top-sec-thumb">
                            <a href="<?php the_permalink() ?>">
                                <img src="<?php echo get_the_post_thumbnail_url($recent["ID"]) ?>" class="img-blog-thumb">
                            </a>
                        </div>

                        <div class="main-sec-thumb">
                            <h4><a href=" <?php the_permalink() ?>">  <?php the_title() ?>
                            </a>
                            </h4>
                        <h6>
                                <a>       
                                <?php the_time('F jS, Y'); ?>  
                                </a>
                        </h6>
                            <small>
                            <?php  the_excerpt()  ?>
                            </small>
                          <div class="read-more">
                            <a href="<?php the_permalink() ?>"> <span class="btn text">
                                 Read More...
                                </span>
                                <span class="fa fa-arrow-right">  
                    </span>
                           </a> 
                          </div>

                        </div>
                    </div>
                </div>
                <!-- end col md 4 -->

				<?php endwhile; else : ?>

					<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>

				<?php endif; ?>

            </div>
        </div>
    </div>
    <!--END HALAMAN BLOG -->


<?php get_footer() ?>